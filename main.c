
#include <stdint.h>
#include <string.h>
#include <stdio.h>
//#include "nordic_common.h"


#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
//#include "softdevice_handler.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define APP_BLE_CONN_CFG_TAG            1  
#define DEVICE_NAME                     "EMG123456"                               /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */

#define APP_ADV_DURATION                18000                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3    
// for test
#define FILE_NAME   "DU19.TXT"
#define TEST_STRING "SD card example.\r\n"
// for SD card
#include "diskio_blkdev.h"
#include "ff.h"
#include "nrf_block_dev_sdc.h"
#include "SEGGER_RTT.h"

// phan bien do minh viet
uint8_t count =0 ;
uint8_t saadc_UartSend[40]={0};
// 30000 phan tu cho data va 19 phan tu cho timestamp 1 phan tu cuoi cho ki tu \0. 
char EMG_data[30001];
char ADC_char[6];
uint32_t count_sample = 0;

uint16_t count_EMG_data=0;
char* user_information;
// update timestamp flag

uint16_t ADC_value;
bool user_info_written=false;

// for SD card

#define SDC_SCK_PIN     6  ///< SDC serial clock (SCK) pin.
#define SDC_MOSI_PIN    7  ///< SDC serial data in (DI) pin.
#define SDC_MISO_PIN    8  ///< SDC serial data out (DO) pin.
#define SDC_CS_PIN      11  ///< SDC chip select (CS) pin.

BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising); 

static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};
/**
 * @brief  SDC block device definition
 * */
NRF_BLOCK_DEV_SDC_DEFINE(
        m_block_dev_sdc,
        NRF_BLOCK_DEV_SDC_CONFIG(
                SDC_SECTOR_SIZE,
                APP_SDCARD_CONFIG(SDC_MOSI_PIN, SDC_MISO_PIN, SDC_SCK_PIN, SDC_CS_PIN)
         ),
         NFR_BLOCK_DEV_INFO_CONFIG("Nordic", "SDC", "1.00")
);


static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
            // LED indication will be changed when advertising starts.
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}
//
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}
//

static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}

//
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}
//

static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static void nus_data_handler(ble_nus_evt_t * p_evt)
{

    if (p_evt->type == BLE_NUS_EVT_RX_DATA)
    {
        uint32_t err_code;

        NRF_LOG_DEBUG("Received data from BLE NUS. Writing data on UART.");
        NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);

        for (uint32_t i = 0; i < p_evt->params.rx_data.length; i++)
        {
            do
            {
                err_code = app_uart_put(p_evt->params.rx_data.p_data[i]);
                if ((err_code != NRF_SUCCESS) && (err_code != NRF_ERROR_BUSY))
                {
                    NRF_LOG_ERROR("Failed receiving NUS message. Error 0x%x. ", err_code);
                    APP_ERROR_CHECK(err_code);
                }
            } while (err_code == NRF_ERROR_BUSY);
        }
        if (p_evt->params.rx_data.p_data[p_evt->params.rx_data.length - 1] == '\r')
        {
            while (app_uart_put('\n') == NRF_ERROR_BUSY);
        }
    }

}

static void services_init(void)
{
    uint32_t           err_code;
    ble_nus_init_t     nus_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}
//
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}
//
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;
        default:
            break;
    }
}

//
/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t               err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance = false;
    init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.srdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;
    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}
//
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}



static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

//

static uint32_t data_buffer;


// phan nay bo sung cho SAADC
// lam viec o che do scan mode 

#define SAADC_SAMPLES_IN_BUFFER         2// SAMPLES_IN_BUFFER is 6 you will get: ch0, ch1, ch2, ch0, ch1, ch2. so SAADC_SAMPLES_IN_BUFFER dat bang voi so kenh ADC.

void saadc_init(void)
{
		NRF_SAADC->RESOLUTION = 2; // 12 BIT MODE
		NRF_SAADC->OVERSAMPLE = 0; //bypass oversample
		
		
		NRF_SAADC->CH[0].CONFIG = 0x0 << 0UL | 0x0 << 4UL | 0x0 << 8UL | 0x0 << 12UL | 0x2 << 16UL;
		NRF_SAADC->CH[0].PSELP = 1; // NRF_SAADC_INPUT_AIN0
		NRF_SAADC->CH[0].PSELN = 0; // NRF_SAADC_INPUT_DISABLED
		
		NRF_SAADC->INTEN = 0x00000;

		
		NRF_SAADC->RESULT.PTR = (uint32_t)(&data_buffer);
		NRF_SAADC->RESULT.MAXCNT = 1; // number of samples
}

void saadc_start()
{
		NRF_SAADC->ENABLE = 1;
    //start task;
    NRF_SAADC->TASKS_START = 0x01UL;
    while (!NRF_SAADC->EVENTS_STARTED){} 
		NRF_SAADC->EVENTS_STARTED = 0;
    NRF_SAADC->TASKS_SAMPLE = 1;
}
// ham ngat timer
//void TIMER2_IRQHandler(void)
//{
//if ((NRF_TIMER2->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER2->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
//  {

//		NRF_TIMER2->EVENTS_COMPARE[0] = 0;           //Clear compare register 0 event		
//		saadc_start();
//		while(!NRF_SAADC->EVENTS_END){}
//		NRF_SAADC->EVENTS_END = 0;
//		if(((uint16_t)data_buffer)>>15)
//		{
//			data_buffer=data_buffer & 0xffff0000;
//		}
//			char ADC_char[6];//khai bap mang ADC_char c� 6 phan tu
//			uint16_t ADC_value;//khai bao ADC_vlue int 16 bit
//	  		//lay gia tri ADC thu duoc
//			ADC_value=(uint8_t)(data_buffer&0x3f)+(uint8_t)((data_buffer>>6UL)&0x3f)*64;
//			//ADC_value= count_sample;
//			//NRF_LOG_INFO("ADC value: %d", ADC_value);
//			//chuyen thanh kieu ki tu
//     		 	sprintf(ADC_char,"\n%d",ADC_value);
//			int sizeADC_char=strlen(ADC_char);//tinh do dai cua ADC_char
//			for(int i=count_EMG_data;i<count_EMG_data+sizeADC_char;i++)
//			{
//				EMG_data[i]=ADC_char[i-count_EMG_data];
//				
//			}
//			// increase buffer with size ADC_char
//			count_EMG_data=count_EMG_data+sizeADC_char;
//			count_sample=count_sample+1;
// }
//}	 

void TIMER2_IRQHandler(void)
{
if ((NRF_TIMER2->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER2->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {

		NRF_TIMER2->EVENTS_COMPARE[0] = 0;           //Clear compare register 0 event		
		saadc_start();
		while(!NRF_SAADC->EVENTS_END){}
		NRF_SAADC->EVENTS_END = 0;
		if(((uint16_t)data_buffer)>>15)
		{
			data_buffer=data_buffer & 0xffff0000;
		}
		
		
		saadc_UartSend[count] = data_buffer&0x3f;
		saadc_UartSend[count+1] = ((data_buffer>>6UL)&0x3f)|0x40;
		
		count=count+2;
		// send data khi thu du 5 goi
		if(count==40)
			{
			 count=0;
				//sizeof(EMG_data)
			//	uint16_t size = sizeof(saadc_UartSend);
			//ble_nus_data_send(&m_nus,,&size,m_conn_handle);
				uint16_t length = (uint16_t)40;
				ble_nus_data_send(&m_nus, saadc_UartSend, &length, m_conn_handle);
		   //ble_nus_string_send();
      }
 }
}	 
void timer_init()
{
	NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;  // Set the timer in Counter Mode // 0x4000A000
  NRF_TIMER2->TASKS_CLEAR = 1;               // clear the task first to be usable for later
	NRF_TIMER2->PRESCALER = 6;                             //Set prescaler. Higher number gives slower timer. Prescaler = 0 gives 16MHz timer
	NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_08Bit;		 //Set counter to 16 bit resolution
	NRF_TIMER2->CC[0] = 10;                                //Set value for TIMER2 compare register 0

	NRF_TIMER2->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
  NVIC_SetPriority(TIMER2_IRQn, 6);
	NVIC_EnableIRQ(TIMER2_IRQn);
	NRF_TIMER2->TASKS_START = 1;  	          // Start TIMER2 	          // Start TIMER2
}

//FATFS
static void fatfs_example()
{
    static FATFS fs;  // file system object structure
    static DIR dir;   // 
    static FILINFO fno;// file information structure
    static FIL file;   // file object structure

    uint32_t bytes_written;
    FRESULT  ff_result;
    DSTATUS  disk_state = STA_NOINIT;

    // Initialize FATFS disk I/O interface by providing the block device.
	  //  
    static diskio_blkdev_t drives[] =
    {
            DISKIO_BLOCKDEV_CONFIG(NRF_BLOCKDEV_BASE_ADDR(m_block_dev_sdc, block_dev), NULL)
    };

    diskio_blockdev_register(drives, ARRAY_SIZE(drives));

    NRF_LOG_INFO("Initializing disk 0 (SDC)...\r\n");
    for (uint32_t retries = 3; retries && disk_state; --retries)
    {
			
        NRF_LOG_INFO(".");
        disk_state = disk_initialize(0);

    }
    if (disk_state)
    {
        NRF_LOG_INFO("Disk initialization failed.\r\n");
        return;
    }

    uint32_t blocks_per_mb = (1024uL * 1024uL) / m_block_dev_sdc.block_dev.p_ops->geometry(&m_block_dev_sdc.block_dev)->blk_size;
    uint32_t capacity = m_block_dev_sdc.block_dev.p_ops->geometry(&m_block_dev_sdc.block_dev)->blk_count / blocks_per_mb;
    NRF_LOG_INFO("Capacity: %d MB\r\n", capacity);

    NRF_LOG_INFO("Mounting volume...\r\n");
    ff_result = f_mount(&fs,"", 1);// mount is a process which the processor allow user can access file and folder.
    if (ff_result)
    {
        NRF_LOG_INFO("Mount failed.\r\n");
        return;
    }

    NRF_LOG_INFO("\r\n Listing directory: /\r\n");
    ff_result = f_opendir(&dir, "/");
    if (ff_result)
    {
        NRF_LOG_INFO("Directory listing failed!\r\n");
        return;
    }    
    do
    {
        ff_result = f_readdir(&dir, &fno); // hai thong so nhap vao la pointer thu muc dich, va pointer thong tin file 
        if (ff_result != FR_OK)
        {
            NRF_LOG_INFO("Directory read failed.");
            return;
        }
        if (fno.fname[0])
        {
            if (fno.fattrib & AM_DIR)// neu day la file cho truy cap truc tiep
            {
                NRF_LOG_RAW_INFO("   <DIR>   %s\r\n",(uint32_t)fno.fname);
            }
            else
            {
                NRF_LOG_RAW_INFO("%9lu  %s\r\n", fno.fsize, (uint32_t)fno.fname);
            }
        }
    }
    while (fno.fname[0]);// neu ten file co ton tai thi lam dieu tren 
    NRF_LOG_RAW_INFO("\r\n");

    NRF_LOG_INFO("Writing to file " FILE_NAME "...\r\n");
  
//    char SD_card_filename[13];
//  	sprintf(SD_card_filename,"%d.txt",use_ID);	
// 	  printf(SD_card_filename,"%s\n");
    ff_result = f_open(&file,FILE_NAME, FA_READ | FA_WRITE | FA_OPEN_APPEND);
		
    if (ff_result != FR_OK)
    {
        NRF_LOG_INFO("Unable to open or create file.\r\n");
        return;
    }
		NRF_LOG_INFO("Successfull open file");
		// thong tin cua user se chi luu lan dau tien.
		if(user_info_written)
		{		
				ff_result = f_write(&file, EMG_data, sizeof(EMG_data), (UINT *) &bytes_written);// this is the function write string into file
				
				if (ff_result != FR_OK)
				{
						NRF_LOG_INFO("Write failed\r\n.");
				}
				else
				{
						NRF_LOG_INFO("%d bytes written.\r\n", bytes_written);
				}
		}
		    
		ff_result = f_write(&file, EMG_data, strlen(EMG_data), (UINT *) &bytes_written);// this is the function write string into file
    
		if (ff_result != FR_OK)
    {
        NRF_LOG_INFO("Write failed\r\n.");
    }
    else
    {
        NRF_LOG_INFO("%d bytes written.\r\n", bytes_written);
    }
    (void) f_close(&file);
		NRF_LOG_INFO("close file thanh cong");
    return;
}


static void advertising_start(void)
{
    uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}

int main(void)
{
		//bool erase_bonds;
		
//	nrf_gpio_cfg_output(4);
//	nrf_gpio_cfg_output(19);
//	nrf_gpio_pin_set(19);
	
		NRF_LOG_INFO("a");
    // Initialize.
		log_init();
		timer_init();
		power_management_init();
		ble_stack_init();
		gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
    advertising_start();
		saadc_init();
	
		  // Enter main loop.
//		while (true)
//		{
//			if(count_sample == 1000)
//				 {
//					nrf_gpio_pin_toggle(4);
//					//NRF_LOG_INFO("a"); 			 
//					count_sample=0;	
//          count_EMG_data=0;	
//					//fatfs_example();					 
//		      }
//		     //power_manage();
//		}
		while(1)
		{
				NRF_LOG_INFO("a");//nrf_gpio_pin_toggle(4);
				//nrf_delay_ms(1000);
			//power_manage();
		}

		
}
		
